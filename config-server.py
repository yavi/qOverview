#!/usr/bin/env python3

import json
import os
import shlex
import subprocess as sp

import dbus
import dbus.mainloop.glib
import dbus.service
import yaml
from gi.repository import GLib

config_dir = os.environ.get('XDG_CONFIG_DIR', os.path.expanduser('~/.config'))
config_file = os.path.join(config_dir, 'qoverview.yaml')

tmp_dir = os.path.join(os.environ.get('XDG_RUNTIME_DIR', '/tmp'), 'qoverview')

search_paths = [os.path.expanduser('~/.local/share/applications'),
                '/usr/share/applications']

with open(config_file) as f:
    options = yaml.safe_load(f)


def uniq(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


def _get_desktop_env():
    desktop_session = os.environ.get('XDG_CURRENT_DESKTOP', os.environ.get('DESKTOP_SESSION'))

    if desktop_session is not None:
        desktop_session = desktop_session.lower()

        # Fix for X-Cinnamon etc
        if desktop_session.startswith('x-'):
            desktop_session = desktop_session.replace('x-', '', 1)

        if desktop_session == 'kde':
            return desktop_session

        elif desktop_session.startswith('kubuntu'):
            return 'kde'

    if os.environ.get('KDE_FULL_SESSION') == 'true':
        return 'kde'

    return 'unsupported'


def _get_background():
    if options.get('background-image', '{DESKTOP_WALLPAPER}') != '{DESKTOP_WALLPAPER}':
        return options['background-image']

    desktop_env = _get_desktop_env()

    if desktop_env == 'kde':
        conf_file = os.path.join(config_dir, 'plasma-org.kde.plasma.desktop-appletsrc')

        line_count = 0
        line_found = 0

        with open(conf_file) as file:
            contents = file.read().splitlines()

        for line in contents:
            line_count += 1

            if '[Wallpaper]' in line and line.startswith('['):
                line_found = int(line_count)
                break

        if line_found != 0:
            contents = contents[line_found:]

            for line in contents:
                if line.startswith('Image'):
                    return line.split('=', 1)[-1].strip().replace('file://', '').replace('"', '').replace("'", '')


def _get_dock_items():
    items = options.get('dock-items', None)

    if isinstance(items, list):
        if len(items) == 1:
            return [items]

        else:
            return items

    else:
        return None


def _desktop_entry_locate(desktop_file_name):
    for s_path in search_paths:
        for file in os.listdir(s_path):
            # reverse, to be able to split on last '.', then get the last item, reversed
            # all this so that we can split on the last '.'
            if file[::-1].split('.', 1)[-1][::-1] == desktop_file_name or file == desktop_file_name:
                return os.path.join(s_path, file)

    # Still not found?
    print('Application (desktop entry) {} not found'.format(desktop_file_name))


def _desktop_entry_execute(desktop_file, return_cmd=False, background=True):
    # Attempt to manually parse and execute

    desktop_entry = _get_desktop_entry_info(desktop_file)

    desktop_file_exec = desktop_entry['Exec']

    for i in desktop_file_exec.split():
        if i.startswith('%'):
            desktop_file_exec = desktop_file_exec.replace(i, '')

    desktop_file_exec = desktop_file_exec.replace(r'%F', '')
    desktop_file_exec = desktop_file_exec.replace(r'%f', '')

    if desktop_entry['Terminal']:
        # Included script: sensible-terminal.sh
        if 'sensible-terminal.sh' in os.listdir('.'):
            desktop_file_exec = 'sh ./sensible-terminal.sh {}'.format(shlex.quote(desktop_file_exec))
        else:
            desktop_file_exec = 'sh /usr/lib/qoverview/sensible-terminal.sh {}'.format(shlex.quote(desktop_file_exec))

    if return_cmd:
        # Only return command to execute
        return desktop_file_exec

    desktop_file_proc = sp.Popen([desktop_file_exec], shell=True)

    if not background:
        desktop_file_proc.wait()


def _get_desktop_entry_info(desktop_file_path):
    desktop_entry_info = {}

    done = []

    with open(desktop_file_path) as file:
        for line in file:
            for key in ['Name', 'Icon', 'Description', 'Exec', 'Terminal']:
                if line.startswith(key + '=') and key not in done:
                    desktop_entry_info[key] = line.split('=', 1)[1].strip()
                    done.append(key)

    if 'Terminal' in desktop_entry_info:
        desktop_entry_info['Terminal'] = True if desktop_entry_info['Terminal'] == 'true' else False

    else:
        desktop_entry_info['Terminal'] = False

    desktop_entry_info['IconPath'] = _get_icon(desktop_entry_info.get('Icon', ''))

    return desktop_entry_info


# noinspection PyArgumentList
def _get_icon(icon_name, categories=('apps', 'actions', 'preferences', )):
    if not icon_name:
        if 'missing-icon.svg' in os.listdir():
            return 'missing-icon.svg'

        else:
            return '/usr/lib/qoverview/missing-icon.svg'

    if os.path.isfile(icon_name):
        return icon_name

    search_icon_paths = [os.path.expanduser('~/.icons'), os.path.expanduser('~/.local/share/icons'), '/usr/share/icons']

    desktop_env = _get_desktop_env()

    if options.get('icon-theme', '{SYSTEM_THEME}') != '{SYSTEM_THEME}':
        theme = options['icon-theme']

    else:  # {SYSTEM_THEME}
        if desktop_env == 'kde':
            theme = sp.check_output(
                ['kreadconfig5', '--group', 'Icons', '--key', 'Theme', '--default', 'breeze']).decode('utf-8')
        else:
            print(
                'Cannot detect system icon theme; falling back to hicolor. Try setting an icon theme in the settings.')
            theme = 'hicolor'

    theme = theme.strip()

    for s_path in search_icon_paths:
        for cat in categories:
            for size in ['64', '48', '32', '24', '22', '12']:
                for ext in ['.svg', '.png']:
                    if theme == 'hicolor':
                        if os.path.exists(
                                os.path.join(s_path, 'hicolor', '{}x{}'.format(size, size), cat, icon_name + ext)):
                            return os.path.join(s_path, 'hicolor', '{}x{}'.format(size, size), cat, icon_name + ext)

                    else:
                        if os.path.exists(os.path.join(s_path, theme, cat, size, icon_name + ext)):
                            return os.path.join(s_path, theme, cat, size, icon_name + ext)

    # Let's try /usr/share/pixmaps and ~/.local/share/pixmaps

    for s_path in [os.path.expanduser('~/.local/share/pixmaps'), '/usr/share/pixmaps']:
        if os.path.isdir(s_path):
            for ext in ['.svg', '.png', '.xpm', '.jpg']:
                if icon_name + ext in os.listdir(s_path):
                    return os.path.join('/usr/share/pixmaps', icon_name + ext)

    # Let's try hicolor

    for s_path in search_icon_paths:
        for cat in categories:
            for size in ['64', '48', '32', '22']:
                for ext in ['.svg', '.png']:
                    if os.path.exists(os.path.join(s_path, 'hicolor', '{}x{}'.format(size, size), cat, icon_name + ext)):
                        return os.path.join(s_path, 'hicolor', '{}x{}'.format(size, size), cat, icon_name + ext)

    # Does the icon_name have an extension (happens...)

    if icon_name[::-1].split('.', 1)[0][::-1] in ['svg', 'png', 'xpm', 'jpg']:
        return _get_icon(icon_name[::-1].split('.', 1)[-1][::-1], categories=categories)

    # Still no icon?
    # Fallback to included missing-icon.svg

    print('No icon found for {}; falling back to missing-icon.svg'.format(icon_name))

    if 'missing-icon.svg' in os.listdir():
        return 'missing-icon.svg'

    else:
        return '/usr/lib/qoverview/missing-icon.svg'


apps_list = []

desktop_entries = []

for path in search_paths:
    for entry in os.listdir(path):
        if entry.endswith('.desktop'):  # some distros (eg. KDE Neon) keep .distrib files here
            if os.path.isfile(os.path.join(path, entry)):
                desktop_entries.append(entry.replace('.desktop', ''))

desktop_entries = uniq(desktop_entries)

for entry in desktop_entries:
    desktop_entry_loc = _desktop_entry_locate(entry)
    if os.path.isfile(desktop_entry_loc):
        info = _get_desktop_entry_info(desktop_entry_loc)
        info['EntryName'] = entry
        apps_list.append(info)


class Service(dbus.service.Object):

    def __init__(self):
        super().__init__()
        self._loop = None

    def run(self):
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        bus_name = dbus.service.BusName("org.qoverview.config", dbus.SessionBus())
        dbus.service.Object.__init__(self, bus_name, "/org/qoverview/config")

        self._loop = GLib.MainLoop()
        self._loop.run()

    @dbus.service.method("org.qoverview.config.iface")
    def get_apps_list(self):
        return json.dumps(apps_list)

    @dbus.service.method("org.qoverview.config.iface")
    def get_icon(self, icon_name):
        return _get_icon(icon_name)

    @dbus.service.method("org.qoverview.config.iface", in_signature='s')
    def desktop_entry_locate(self, desktop_file):
        return _desktop_entry_locate(desktop_file)

    @dbus.service.method("org.qoverview.config.iface")
    def desktop_entry_info(self, desktop_file):
        return json.dumps(_get_desktop_entry_info(desktop_file))

    @dbus.service.method("org.qoverview.config.iface")
    def desktop_entry_execute(self, desktop_file):
        _desktop_entry_execute(desktop_file, background=True)
        print('Executed "{}".'.format(desktop_file))

    @dbus.service.method("org.qoverview.config.iface")
    def get_background(self):
        print('argh')
        return _get_background().replace('file://', '')

    @dbus.service.method("org.qoverview.config.iface")
    def get_dock_items(self):
        return json.dumps(_get_dock_items() or [])

    @dbus.service.method("org.qoverview.config.iface")
    def get_config(self):
        return json.dumps(options)


if __name__ == '__main__':
    Service().run()
